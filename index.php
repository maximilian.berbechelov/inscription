<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='public/css/app.css' rel='stylesheet' />
	<title>Inscription Films</title>
	<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
	<style>
		/* Fonts */
		@import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);
		@import url('https://fonts.googleapis.com/css?family=Kanit:100,200,300,400|Quicksand:300,400');


		.font-qs {
			font-family: Quicksand;
		}
	</style>
</head>

<body>
	<div class='flex items-center justify-center h-screen'>
		<div class='max-w-6xl my-auto mx-auto'>
			<div class='name'><img src='images/logo_blue_noeye.png' alt='Inscription Ltd' /></div>
			<div class='-mt-6 font-qs'>by Maximilian Berbechelov</div>
		</div>
	</div>
</body>

</html>